import java.util.LinkedList;
import java.util.List;

public class Wiskunde implements Vak {

    private List<Integer> punten;

    public Wiskunde() {
        this.punten = new LinkedList<Integer>();
    }

    @Override
    public String getNaam() {
        return "Wiskunde";
    }

    @Override
    public List<Integer> getPunten() {
        return punten;
    }
}
