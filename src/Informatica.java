import java.util.LinkedList;
import java.util.List;

public class Informatica implements Vak {

    private List<Integer> punten;

    public Informatica() {
        this.punten = new LinkedList<>();
    }

    @Override
    public String getNaam() {
        return "Informatica";
    }

    @Override
    public List<Integer> getPunten() {
        return punten;
    }
}
