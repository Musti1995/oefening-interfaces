import java.util.LinkedList;
import java.util.List;

public class Aardrijkskunde implements Vak {

    private List<Integer> punten;

    public Aardrijkskunde() {
        this.punten = new LinkedList<>();
    }

    @Override
    public String getNaam() {
        return "Aardrijkskunde";
    }

    @Override
    public List<Integer> getPunten() {
        return punten;
    }
}
