import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Rapport {

    private static Scanner scanner;
    private static List<Vak> vakken;
    private static Vak aard, frans, gesch, info, ned, wisk;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        vakken = new LinkedList<>();
        aard = new Aardrijkskunde();
        frans = new Frans();
        gesch = new Geschiedenis();
        info = new Informatica();
        ned = new Nederlands();
        wisk = new Wiskunde();
        vakken.add(aard);
        vakken.add(frans);
        vakken.add(gesch);
        vakken.add(info);
        vakken.add(ned);
        vakken.add(wisk);
        System.out.println("Typ \"help\" voor een overzicht van commando's");
        String input;
        while ((input = scanner.nextLine()) != null) {
            input = input.toLowerCase();
            String[] tokens = input.split(" ");
            switch(tokens[0]) {
                case "help":
                    System.out.println("Lijst van commando's:");
                    System.out.format("%-20s Toont dit overzicht\n", "HELP");
                    System.out.format("%-20s Toont een overzicht van het rapport\n", "TOON RAPPORT");
                    System.out.format("%-20s Toont een overzicht van het ingegeven vak\n", "TOON VAK <vak>");
                    System.out.format("%-20s Voegt <score> toe aan de lijst van punten voor het ingegeven vak\n", "VAK+ <vak> <score>");
                    System.out.format("%-20s Wist alle punten voor het ingegeven vak (gebruik ALL voor alle vakken)\n", "VAK- <vak>");
                    System.out.format("%-20s Sluit het programma", "EXIT");
                    System.out.println();
                    continue;
                case "toon":
                    if (tokens.length <= 1) continue;
                    if (tokens[1].equals("rapport")) toonRapport();
                    if (tokens.length <= 2) continue;
                    if (tokens[1].equals("vak")) toonVak(tokens[2]);
                    continue;
                case "vak+":
                    if (tokens.length <= 2) continue;
                    voegPuntenToe(tokens[1], tokens[2]);
                    continue;
                case "vak-":
                    if (tokens.length <= 1) continue;
                    wisPunten(tokens[1]);
                    continue;
                case "exit":
                    System.exit(0);
                    break;
            }
        }
    }

    static Vak bepaalVak(String input) {
        if (input.startsWith("aard")) return aard;
        if (input.startsWith("frans")) return frans;
        if (input.startsWith("ges")) return gesch;
        if (input.startsWith("info")) return info;
        if (input.startsWith("ned")) return ned;
        if (input.startsWith("wisk")) return wisk;
        return null;
    }

    static void wisPunten(String v) {
        if (v.equals("all")) {
            for (Vak vak : vakken) vak.getPunten().clear();
            return;
        }
        Vak vak = bepaalVak(v);
        if (vak == null) return;
        vak.getPunten().clear();
    }

    static void voegPuntenToe(String v, String punten) {
        Vak vak = bepaalVak(v);
        if (vak == null) return;
        String[] tokens = punten.split(",");
        for (String s : tokens) vak.getPunten().add(Integer.parseInt(s));
    }

    static void toonVak(String v) {
        toonVak(bepaalVak(v));
    }

    static void toonVak(Vak vak) {
        if (vak == null) return;
        float gem = rondAf(berekenGemiddelde(vak));
        float med = rondAf(berekenMediaan(vak));
        System.out.printf("%15s | G: %3.1f | M: %3.1f | ", vak.getNaam(), gem, med);
        for (int p : vak.getPunten()) System.out.printf("%4d", p);
        System.out.println();
    }

    static void toonRapport() {
        for (Vak vak : vakken) toonVak(vak);
    }

    static float berekenGemiddelde(Vak vak) {
        if (vak.getPunten().size() == 0) return 0f;
        float som = 0;
        for (int s : vak.getPunten()) som += s;
        return som / vak.getPunten().size();
    }

    static float berekenMediaan(Vak vak) {
        if (vak.getPunten().size() == 0) return 0f;
        int getallen = vak.getPunten().size();
        if (getallen % 2 == 0) {
            getallen /= 2;
            return (vak.getPunten().get(getallen-1) + vak.getPunten().get(getallen))/2f;
        } else {
            return vak.getPunten().get((int) getallen / 2);
        }
    }

    static float rondAf(float x) {
        x *= 10;
        x = Math.round(x);
        return x / 10f;
    }
}
