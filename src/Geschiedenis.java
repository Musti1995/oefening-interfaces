import java.util.LinkedList;
import java.util.List;

public class Geschiedenis implements Vak {

    private List<Integer> punten;

    public Geschiedenis() {
        this.punten = new LinkedList<>();
    }

    @Override
    public String getNaam() {
        return "Geschiedenis";
    }

    @Override
    public List<Integer> getPunten() {
        return punten;
    }
}
