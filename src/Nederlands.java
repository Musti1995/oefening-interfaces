import java.util.LinkedList;
import java.util.List;

public class Nederlands implements Vak {

    private List<Integer> punten;

    public Nederlands() {
        this.punten = new LinkedList<>();
    }

    @Override
    public String getNaam() {
        return "Nederlands";
    }

    @Override
    public List<Integer> getPunten() {
        return punten;
    }
}
