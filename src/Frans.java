import java.util.LinkedList;
import java.util.List;

public class Frans implements Vak {

    private List<Integer> punten;

    public Frans() {
        this.punten = new LinkedList<>();
    }

    @Override
    public String getNaam() {
        return "Frans";
    }

    @Override
    public List<Integer> getPunten() {
        return punten;
    }
}
